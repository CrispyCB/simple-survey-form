function loadCountries() {
  const countries = [
    "US",
    "UK",
    "Canada",
    "Mexico",
    "France",
    "Germany",
    "Belgium",
    "Switzerland",
    "Netherlands",
    "Romania",
    "Luxembourg",
    "Spain",
    "Italy",
    "Sweden",
    "Denmark",
    "Norway",
    "Finland",
    "Russia",
    "China",
    "Japan",
  ];
  const country_dropdown = document.getElementById("country_dropdown");

  countries.forEach((country) => {
    let option = document.createElement("option");
    option.value = country;
    option.textContent = country;
    country_dropdown.appendChild(option);
  });
}

loadCountries();
